(ns vidownload.core
  (:require
   [clojure.java.io :as io]
   [clojure.string :as s]
   [clojure.java.shell :refer [sh with-sh-dir]]
   [vidownload.network :as net]))

(defn combine-subtitles
  [dir n]
  (let [filespec (format "%s/subtitles.vtt" dir)]
    (when (.exists (io/file filespec))
      (with-open [out (io/writer filespec)]
        (dotimes [i n]
          (let [fspec (format "%s/%d.vtt" dir i)
                lines (line-seq (io/reader fspec))]
            (doseq [line (if (zero? i) lines (nthrest lines 6))]
              (.write out (format "%s\n" line)))))))))

(defn generate-ffmpeg-input-file
  [dir n]
  (with-open [out (io/output-stream (io/file (format "%s/segments.txt" dir)))]
    (io/copy (s/join "\n" (sort
                           (map #(format "file '%s'" (.getName %))
                                (get-files-with-extension dir ".ts"))))
             out)))

(defn get-files-with-extension
  [dir extension]
  (filter #(s/ends-with? % extension) (file-seq (io/file dir))))

(defn -main
  [& args]
  (let [{:keys [dir n title url]} args
        dir (format "%s/ep%02d" dir n)]
    (net/download-episode dir url)
    (combine-subtitles dir (count (get-files-with-extension dir ".vtt")))
    (generate-ffmpeg-input-file dir (count (get-files-with-extension dir ".ts")))
    (with-sh-dir dir
      (sh "ffmpeg" "-f" "concat" "-safe" "0" "-i" "segments.txt" "-c" "copy"
          (format "%02d-%s.mp4" n (s/join "-" (s/split title #" ")))))))
