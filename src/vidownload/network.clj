(ns vidownload.network
  (:require
   [clj-http.client :as http]
   [clojure.java.io :as io]
   [clojure.string :as s]))

(def ^:dynamic *outdir* ".")
(def ^:dynamic *reload?* false)
(def ^:dynamic *default-bandwidth* 2493700)

(defn is-file?
  [filespec]
  (.exists (io/file filespec)))

(defn write-binary-file
  [filespec data]
  (with-open [outstream (io/output-stream (io/file filespec))]
    (io/copy data outstream)))

(defn maybe-make-directory
  [dirspec]
  (.mkdirs (io/file dirspec)))

(defn get-body
  [url & args]
  (:body (apply http/get url args)))

(defn save-body
  [filespec url]
  (when-not (is-file? filespec)
    (if (s/ends-with? filespec ".ts")
      (write-binary-file filespec (get-body url {:as :byte-array :content-type "video/MP2T"}))
      (spit filespec (get-body url))))
  filespec)

(defn url-filespec
  [url]
  (last (s/split (first (s/split url #"\?")) #"\/")))

(defn download-from-url
  [url]
  (let [fullpath (str *outdir* "/" (url-filespec url))]
    (when-not (and (not *reload?*) (is-file? fullpath))
      (println "downloading" fullpath)
      (save-body fullpath url))
    fullpath))

(defn download
  [dir url i]
  (try
    (binding [*outdir* dir]
      (println "\ndownload attempt" i "for" (url-filespec url))
      (download-from-url url))
    (catch Exception _
      (when (< i 10)
        (download dir url (inc i))))))

(defn download-playlist-segments
  [playlist]
  (with-open [f (io/reader playlist)]
    (doseq [line (line-seq f)]
      (when (s/starts-with? line "https")
        (download *outdir* line 0)))))

(defn subtitle-line?
  [line]
  (let [lowercase-line (s/lower-case line)]
    (and (s/starts-with? lowercase-line "#ext-x-media")
         (s/includes? lowercase-line "type=subtitles"))))

(defn video-line?
  [last-line line]
  (let [lowercase-line (s/lower-case line)
        lowercase-last-line (s/lower-case last-line)]
    (and (s/starts-with? lowercase-last-line "#ext-x-stream-inf")
         (s/includes? lowercase-last-line (format "bandwidth=%d" *default-bandwidth*))
         (s/starts-with? lowercase-line "https"))))

(defn split-attributes
  [line]
  (map (fn [x] (s/split x #"=" 2))
       (s/split (last (s/split line #":" 2)) #",")))

(defn attrs->map
  [line]
  (let [attrs (split-attributes line)]
    (zipmap (map #(keyword (s/lower-case (first %1))) attrs)
            (map #(let [x (second %1)]
                    (if (s/starts-with? x "\"")
                      (read-string x)
                      x))
                 attrs))))

(defn get-playlist
  [filespec playlist-type]
  (case playlist-type
    :subtitle (some #(when (subtitle-line? %)
                       (download *outdir* (:uri (attrs->map %)) 0))
                    (line-seq (io/reader filespec)))
    :video (let [lines (line-seq (io/reader filespec))]
             (some #(let [[last-line curr-line] %]
                      (when (video-line? last-line curr-line)
                        (download *outdir* curr-line 0)))
                   (mapv #(vec [%1 %2]) lines (rest lines))))))

(defn download-episode
  [dir url]
  (maybe-make-directory dir)
  (binding [*outdir* dir]
    (let [master-playlist (download-from-url url)
          subtitles-playlist (get-playlist master-playlist :subtitle)
          video-playlist (get-playlist master-playlist :video)]
      (if subtitles-playlist
        (download-playlist-segments subtitles-playlist)
        (printf "no subtitles in %s\n" master-playlist))
      (if video-playlist
        (download-playlist-segments video-playlist)
        (printf "no video in %s\n" video-playlist)))))
